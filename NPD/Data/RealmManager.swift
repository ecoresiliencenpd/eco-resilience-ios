//
//  RealmManager.swift
//  NPD
//
//  Created by Alejandro Tapia on 10/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    
    static func updateItself(object: Itself) -> Bool {
        do {
            let real = try Realm()
            try real.write { real.add(object) }
            return true
        } catch let error as NSError {
            print("Realm error: \(error.description)")
            return false
        }
    }
    
    static func updateCatalog(objects: [Object]) {
        do {
            let real = try Realm()
            try real.write {
                for object in objects {
                    real.add(object)
                }
            }
        } catch let error as NSError {
            print("Realm error: \(error.description)")
        }
    }
    
    static func fetchStatesCatalog() -> Results<State>? {
        do {
            let realm = try Realm()
            let states = realm.objects(State.self)
            if states.count == 0 { return nil }
            return states
        } catch let error as NSError {
            print("Realm error: \(error.description)")
            return nil
        }
    }
    
    static func fetchDistrictsCatalog() -> Results<District>? {
        do {
            let realm = try Realm()
            let districts = realm.objects(District.self)
            if districts.count == 0 { return nil }
            return districts
        } catch let error as NSError {
            print("Realm error: \(error.description)")
            return nil
        }
    }
    
    static func fetchUniversitiesCatalog() -> Results<University>? {
        do {
            let realm = try Realm()
            let universities = realm.objects(University.self)
            if universities.count == 0 { return nil }
            return universities
        } catch let error as NSError {
            print("Realm error: \(error.description)")
            return nil
        }
    }
    
    static func fetchItself() -> Itself {
        do {
            let realm = try Realm()
            if let itself = realm.objects(Itself.self).first {
             return itself
            }
        } catch let error as NSError {
            print("Realm Fetch Itself Error: \(error.description)")
        }
        return Itself()
    }
    
    static func deleteAll() {
        do{
            let realm = try Realm()
            try realm.write {
                realm.deleteAll()
            }
        } catch let error as NSError {
            print("Realm Delete All Error: \(error.description)")
        }
    }
}
