//
//  RegisterUser.swift
//  NPD
//
//  Created by Alejandro Hernández on 13/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation

class RegisterUser {
    
    // MARK: - Properties
    private var Mail: String?
    private var Password: String?
    private var FirstName: String?
    private var LastName: String?
    private var MiddleName: String?
    private var IdProfile: Int?
    private var ProfileName: String?
    private var IdState: Int?
    private var StateName: String?
    private var IdDistrict: Int?
    private var DistrictName: String?
    private var IdUniversity: Int?
    private var UniversityName: String?
    
    // MARK: - Getters & Setters
    var mail: String? {
        get { return Mail }
        set { Mail = newValue }
    }
    
    var password: String? {
        get { return Password }
        set { Password = newValue }
    }
    
    var firstName: String? {
        get { return FirstName }
        set { FirstName = newValue }
    }
    
    var lastName: String? {
        get { return LastName }
        set { LastName = newValue }
    }
    
    var middleName: String? {
        get { return MiddleName }
        set { MiddleName = newValue }
    }
    
    var idProfile: Int? {
        get { return IdProfile }
        set { IdProfile = newValue }
    }
    
    var profileName: String? {
        get { return ProfileName }
        set { ProfileName = newValue }
    }
    
    var idState: Int? {
        get { return IdState }
        set { IdState = newValue }
    }
    
    var stateName: String? {
        get { return StateName }
        set { StateName = newValue }
    }
    
    var idDistrict: Int? {
        get { return IdDistrict }
        set { IdDistrict = newValue }
    }
    
    var districtName: String? {
        get { return DistrictName }
        set { DistrictName = newValue }
    }
    
    var idUniversity: Int? {
        get { return IdUniversity }
        set { IdUniversity = newValue }
    }
    
    var universityName: String? {
        get { return UniversityName }
        set { UniversityName = newValue }
    }
}
