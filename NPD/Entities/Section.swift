//
//  Section.swift
//  NPD
//
//  Created by Alejandro Tapia on 26/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Section: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var name: String = ""
    var modules = List<Module>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id_section"]
        name <- map["section_name"]
    
        var array = [Module]()
        array <- map["modules"]
        modules.append(objectsIn: array)
    }
}
