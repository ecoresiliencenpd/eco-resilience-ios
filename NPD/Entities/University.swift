//
//  University.swift
//  NPD
//
//  Created by Alejandro Tapia on 09/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class University: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var active: Int = 0
    dynamic var name: String = ""
    dynamic var stateId: Int = 0
    dynamic var districtId: Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id_university"]
        active <- map["active"]
        name <- map["university_name"]
        stateId <- map["id_state"]
        districtId <- map["id_district"]
    }
    
}
