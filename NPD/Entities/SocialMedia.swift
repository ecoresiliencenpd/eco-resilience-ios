//
//  SocialMedia.swift
//  NPD
//
//  Created by Alejandro Tapia on 30/09/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//

import Foundation

class SocialMedia {
    
    var link: String = ""
    var name: String = ""
    
    init(name: String, link: String) {
        self.name = name
        self.link = link
    }
}
