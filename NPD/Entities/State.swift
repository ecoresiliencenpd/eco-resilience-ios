//
//  State.swift
//  NPD
//
//  Created by Alejandro Tapia on 09/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class State: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var active: Int = 0
    dynamic var name: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id_state"]
        active <- map["active"]
        name <- map["state_name"]
    }
}

