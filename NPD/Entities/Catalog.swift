//
//  Catalog.swift
//  NPD
//
//  Created by Alejandro Tapia on 26/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Catalog: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var profileName: String = ""
    var sections = List<Section>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id_profile"]
        profileName <- map["profile_name"]
        
        var array = [Section]()
        array <- map["sections"]
        sections.append(objectsIn: array)
    }
}
