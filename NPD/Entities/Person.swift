//
//  Person.swift
//  NPD
//
//  Created by Alejandro Tapia on 26/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Person: Object, Mappable {
    
    dynamic var firstName: String = ""
    dynamic var middleName: String = ""
    dynamic var lastName: String = ""
    dynamic var email: String = ""
    dynamic var profileName: String = ""
    dynamic var districtName: String = ""
    dynamic var stateName: String = ""
    dynamic var universityName: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        firstName <- map["first_name"]
        middleName <- map["middle_name"]
        lastName <- map["last_name"]
        email <- map["mail"]
        profileName <- map["profile_name"]
        districtName <- map["district_name"]
        stateName <- map["state_name"]
        universityName <- map["university_name"]
    }
}
