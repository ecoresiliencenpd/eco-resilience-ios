//
//  Root.swift
//  NPD
//
//  Created by Alejandro Tapia on 26/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Itself: Object, Mappable {
    
    dynamic var person: Person?
    dynamic var catalog: Catalog?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        person <- map["person"]
        catalog <- map["catalog"]
    }
}

