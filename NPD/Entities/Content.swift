//
//  Content.swift
//  NPD
//
//  Created by Alejandro Tapia on 26/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Content: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var title: String = ""
    var articles = List<Article>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id_title_content"]
        title <- map["title_content"]
        
        var array = [Article]()
        array <- map["articles"]
        articles.append(objectsIn: array)
    }
}

