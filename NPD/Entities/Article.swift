//
//  Article.swift
//  NPD
//
//  Created by Alejandro Tapia on 26/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Article: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var contentDesc: String = ""
    dynamic var lastUpdate: Int = 0
    dynamic var urlImage: String = ""
    dynamic var urlDoc: String = ""

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id_content"]
        contentDesc <- map["content_desc"]
        lastUpdate <- map["last_update"]
        urlImage <- map["url_image"]
        urlDoc <- map["url_doc"]
    }
}
