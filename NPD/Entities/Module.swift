//
//  Module.swift
//  NPD
//
//  Created by Alejandro Tapia on 26/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Module: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var title: String = ""
    var contents = List<Content>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id_title_module"]
        title <- map["title_module"]
        
        var array = [Content]()
        array <- map["contents"]
        contents.append(objectsIn: array)
    }
}
