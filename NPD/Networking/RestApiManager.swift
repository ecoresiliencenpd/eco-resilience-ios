//
//  RestApiManager.swift
//  NPD
//
//  Created by Alejandro Tapia on 08/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation
import Moya

enum RestApiManager {
    case doLogin(userName: String, password: String)
    case getCatDistricts()
    case getCatStates()
    case getCatUniversities()
    case registration(user: RegisterUser)
    case getAppDetail()
}

extension RestApiManager: TargetType {
    
    var baseURL: URL { return URL(string: "http://209.159.146.43:8080/WSEcoResilenceNPD/rest/npdProgram")! }
    
    // MARK: - TargetType Protocol Implementation
    var path: String {
        switch self {
        case .doLogin:
            return "/doLogin"
        case .getCatDistricts:
            return "/getCatDistricts"
        case .getCatStates:
            return "/getCatStates"
        case .getCatUniversities:
            return "/getCatUniversities"
        case .registration:
            return "/registration"
        case .getAppDetail:
            return "/getAppDetail"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .doLogin, .registration:
            return .post
        case .getCatDistricts, .getCatStates, .getCatUniversities, .getAppDetail:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .getCatDistricts, .getCatStates, .getCatUniversities, .getAppDetail:
             return .requestPlain
        case .doLogin(let userName, let password):
            return .requestParameters(
                parameters: ["username": userName, "password": password],
                encoding: JSONEncoding.default)
        case .registration(let user):
            return .requestParameters(
            parameters:
                ["mail": user.mail!,
                 "password": user.password!,
                 "first_name": user.firstName!,
                 "last_name": user.lastName!,
                 "middle_name": user.middleName!,
                 "id_profile": user.idProfile!,
                 "profile_name": user.profileName!,
                 "id_state": user.idState!,
                 "state_name": user.stateName!,
                 "id_district": user.idDistrict!,
                 "district_name": user.districtName!,
                 "id_university": user.idUniversity!,
                 "university_name": user.universityName!
                 ]
            , encoding: JSONEncoding.default)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .doLogin(let userName, let password):
            return "{\"username\": \"\(userName)\", \"password\": \"\(password)\"}".utf8Encoded
        case .registration(let user):
            return "{\"mail\": \"\(user.mail!)\", \"password\": \"\(user.password!)\", \"first_name\": \"\(user.firstName!)\", \"last_name\": \"\(user.lastName!)\", \"middle_name\": \"\(user.middleName!)\", \"id_profile\": \(user.idProfile!), \"profile_name\": \"\(user.profileName!)\", \"id_state\": \(user.idState!), \"state_name\": \"\(user.stateName!)\", \"id_district\" : \(user.idDistrict!), \"district_name\": \"\(user.districtName!)\", \"id_university\": \(user.idUniversity!), \"university_name\": \"\(user.universityName!)\"}".utf8Encoded
        case  .getCatDistricts, .getCatStates, .getCatUniversities, .getAppDetail:
            return "Half measures are as bad as nothing at all.".utf8Encoded
            
        }
    }
    
    public var validate: Bool {
        return false
    }
    
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
