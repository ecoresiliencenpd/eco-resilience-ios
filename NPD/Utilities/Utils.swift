//
//  Utils.swift
//  NPD
//
//  Created by Alejandro Tapia on 08/10/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//

import Foundation

class Utils {
    
    class func checkResourceType(fromUrl url: String) -> ResourceType {
        let lowercased = url.lowercased()
        if lowercased.hasSuffix(Constants.PDF_EXTENSION)    ||
            lowercased.hasSuffix(Constants.DOC_EXTENSION)   ||
            lowercased.hasSuffix(Constants.DOCX_EXTENSION)  ||
            lowercased.hasSuffix(Constants.PNG_EXTENSION)   ||
            lowercased.hasSuffix(Constants.JPG_EXTENSION)   ||
            lowercased.hasSuffix(Constants.PPT_EXTENSION)   ||
            lowercased.hasSuffix(Constants.PPTX_EXTENSION)  ||
            lowercased.hasSuffix(Constants.XLS_EXTENSION)   ||
            lowercased.hasSuffix(Constants.XLSX_EXTENSION) {
            return .doc
        } else {
            return .web
        }
    }
}
