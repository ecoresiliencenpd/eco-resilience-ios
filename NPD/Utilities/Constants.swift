//
//  Constants.swift
//  NPD
//
//  Created by Alejandro Tapia on 26/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation

class Constants {
    
  // REGEX
  static let EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
  static let PASSWORD_REGEX = "^.{6,}$"
  
  // USER DEFAULTS KEYS
  static let USER_DEFAULTS_IS_LOGGED_KEY = "isLogged"
  static let USER_DEFAULTS_USERNAME = "username"
  static let USER_DEFAULTS_PASSWORD = "password"
    
  // PROFILE NAME
  static let PRINCIPAL_PROFILE_NAME = "Principal"
  static let TEACHER_PROFILE_NAME = "Teacher"
  static let PARENT_PROFILE_NAME = "Parent"
  static let STUDENT_PROFILE_NAME = "Student"
  
  // DICTIONARY KEYS
  static let DICTIONARY_KEY_APP_POLICY = "appPolicy"
  static let DICTIONARY_KEY_LINK = "link"
  static let DICTIONARY_KEY_NAME = "name"
  static let DICTIONARY_KEY_CONTACT_SUPPORT = "contactSupport"
  static let DICTIONARY_KEY_MAIL = "mail"
  static let DICTIONARY_KEY_SOCIAL_MEDIA = "socialMedia"
  static let DICTIONARY_KEY_VERSION = "version"
  static let DICTIONARY_KEY_VERSION_APP = "versionApp"
  
  // DOCS EXTENSION
  static let PDF_EXTENSION = ".pdf"
  static let DOC_EXTENSION = ".doc"
  static let DOCX_EXTENSION = ".docx"
  static let PNG_EXTENSION = ".png"
  static let JPG_EXTENSION = ".jpg"
  static let PPT_EXTENSION = ".ppt"
  static let PPTX_EXTENSION = ".pptx"
  static let XLS_EXTENSION = ".xls"
  static let XLSX_EXTENSION = ".xlsx"
}
