//
//  UIFontExtension.swift
//  NPD
//
//  Created by Alejandro Tapia on 19/08/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//

import UIKit

/// NPD Custom Fonts.
extension UIFont {
    
    /// Font SourceSansPro, Bold, 17pt
    /// - Returns: Custom SourceSansPro-Bold font with size 17.0 pt.
    class func npdBold17() -> UIFont? {
        return UIFont(name: "SourceSansPro-Bold", size: 17.0)
    }
    
    /// Font SourceSansPro, Light, 17pt
    /// - Returns: Custom SourceSansPro-Light font with size 17.0 pt.
    class func npdLight17() -> UIFont? {
        return UIFont(name: "SourceSansPro-Light", size: 17.0)
    }
}
