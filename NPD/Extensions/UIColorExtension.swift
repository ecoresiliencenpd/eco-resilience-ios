//
//  UIColorExtension.swift
//  NPD
//
//  Created by Alejandro Tapia on 19/08/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//
//  Colors' name was obtained by: http://www.htmlcsscolor.com/

import UIKit

/// EcoResilence Palette Color
extension UIColor {
    
    /// NPD - Persian Green: rgb(0, 169, 157), #00A99D,  %100
    class var npdPersianGreen: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    
    /// NPD - Atlantis: rgb(140, 198, 63), #8CC63F,  %100
    class var npdAtlantis: UIColor {
        return UIColor(red: 140.0 / 255.0, green: 198.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
    }
    
    /// NPD - Seal Brown: rgb(80, 0, 0), #500000,  %100
    class var npdSealBrown: UIColor {
        return UIColor(red: 80.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    
    /// NPD - Pattens Blue: rgb(222, 225, 226), #DEE1E2,  %100
    class var npdPattensBlue: UIColor {
        return UIColor(red: 222.0 / 255.0, green: 225.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0)
    }
    
    /// NPD - Pink: rgb(189, 151, 151), #BD9797,  %100
    class var npdPink: UIColor {
        return UIColor(red: 189.0 / 255.0, green: 151.0 / 255.0, blue: 151.0 / 255.0, alpha: 1.0)
    }
    
    /// NPD - Blue: rgb(0, 55, 115), #003773,  %100
    class var npdBlue: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 55.0 / 255.0, blue: 115.0 / 255.0, alpha: 1.0)
    }
    
}
