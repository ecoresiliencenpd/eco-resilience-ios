//
//  Enums.swift
//  NPD
//
//  Created by Alejandro Tapia on 09/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import Foundation

enum CatalogType {
    case state, district, university
}

enum ResourceType {
    case doc, web
}
