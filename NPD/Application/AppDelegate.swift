//
//  AppDelegate.swift
//  NPD
//
//  Created by Alex Tapia on 18/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if UserDefaults.standard.bool(forKey: Constants.USER_DEFAULTS_IS_LOGGED_KEY) {
            RootRouter().presentMainScreen(in: window!)
        } else {
            RootRouter().presentLoginScreen(in: window!)
        }
        
        return true
    }
}

