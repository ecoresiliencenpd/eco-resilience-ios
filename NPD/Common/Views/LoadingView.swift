//
//  LoadingView.swift
//  NPD
//
//  Created by Alejandro Tapia on 03/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var blurView: UIView!
    
    // MARK: - Override Methods
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - Internal
    
    func setup() {
        setupView()
        setupBluerView()
    }
    
    func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func setupBluerView() {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = blurView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.addSubview(blurEffectView)
    }
    
    /// Loads first view from XIB file.
    /// - Returns: the first view from xib file.
    func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}
