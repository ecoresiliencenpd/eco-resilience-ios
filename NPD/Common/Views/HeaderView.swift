//
//  HeaderView.swift
//  NPD
//
//  Created by Alejandro Tapia on 11/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    
    @IBOutlet public weak var alphaView: UIView!
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var subtitleLabel: UILabel!
    @IBOutlet public weak var headerTitleLabel: UILabel!
    
    // MARK: - Override Methods
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - Internal
    
    func setup() {
        setupView()
        //setupSubviews()
    }
    
    func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func setupSubviews() {
        titleLabel.text = ""
        subtitleLabel.text = ""
        headerTitleLabel.text = ""
    }
    
    /// Loads first view from XIB file.
    /// - Returns: the first view from xib file.
    func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    
}
