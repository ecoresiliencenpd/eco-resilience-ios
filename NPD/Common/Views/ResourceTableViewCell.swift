//
//  DocTableViewCell.swift
//  NPD
//
//  Created by Alejandro Tapia on 07/10/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import UIKit

class ResourceTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var iconResourceTypeImageView: UIImageView!
    @IBOutlet weak var resourceTypeLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    
    // MARK: - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Public methods
    public func setupCell(withUrl url: String, andResourceType type: ResourceType){
        setupCellfrom(resourceType: type)
        urlLabel.text = url
    }
    
    // MARK: - Internal
    internal func setupCellfrom(resourceType: ResourceType) {
        switch resourceType {
        case .doc:
            iconResourceTypeImageView.image = #imageLiteral(resourceName: "ic_document")
            resourceTypeLabel.text = "Document"
        case .web:
            iconResourceTypeImageView.image = #imageLiteral(resourceName: "ic_web_link")
            resourceTypeLabel.text = "Web"
        }
    }
}
