//
//  DialogViewController.swift
//  NPD
//
//  Created by Alejandro Tapia on 07/10/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//

import UIKit

class DialogViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var universitiesLabel: UILabel!
    
    // MARK: - Public variables
    public var universities = [String]()
    
    // MARK: - Initialization
    init() {
        super.init(nibName: R.nib.dialogViewController.name, bundle: nil)
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setAttributeStringFromUniversities()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Internal
    func setAttributeStringFromUniversities() {
        let combination = NSMutableAttributedString()
        for university in universities {
            let range = NSRange(location: 0, length: 1)
            let attrString = NSMutableAttributedString(string: "· \(university)\n")
            attrString.addAttribute(NSForegroundColorAttributeName, value: #colorLiteral(red: 0.3918663859, green: 0.0311193075, blue: 0, alpha: 1), range: range)
            combination.append(attrString)
        }
        universitiesLabel.attributedText = combination
    }
    
    // MARK: - IBActions
    @IBAction func pressedGoButton() {
        self.dismiss(animated: true, completion: nil)
    }
}
