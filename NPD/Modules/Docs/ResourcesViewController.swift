//
//  DocsViewController.swift
//  NPD
//
//  Created Alejandro Tapia on 07/10/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//
//  VIPER Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class ResourcesViewController: UIViewController {

    // MARK: - VIPER variables
	var presenter: ResourcesPresenterProtocol?
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Public Variables
    public var urls = [String]()

    // MARK: - Lifecycle
	override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 66
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    // MARK: - IBActions
    @IBAction func pressedBackButton() {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableView DataSource & Delegate
extension ResourcesViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return urls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.resourceCell)!
        let url = urls[indexPath.row]
        cell.setupCell(withUrl: url, andResourceType: Utils.checkResourceType(fromUrl: url))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = urls[indexPath.row]
        switch Utils.checkResourceType(fromUrl: url) {
        case .doc:
            presenter?.willPresentDocViewer(withURL: url)
        case .web:
            presenter?.willPresentBrowser(withURL: url)
        }
    }
}
