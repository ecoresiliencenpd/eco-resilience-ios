//
//  DocsRouter.swift
//  NPD
//
//  Created Alejandro Tapia on 07/10/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//
//  VIPER Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class ResourcesRouter: ResourcesWireframeProtocol {
    
  weak var viewController: UIViewController?
  
  static func createModule(withUrls urls: [String]) -> UIViewController {
    guard let navController = R.storyboard.main.resourcesNavigationController() else {
      return UIViewController()
    }
    guard let view = navController.viewControllers.first as? ResourcesViewController else {
      return UIViewController()
    }
    let router = ResourcesRouter()
    let presenter = ResourcesPresenter(router: router)
  
    view.presenter = presenter
    view.urls = urls
  
    router.viewController = view
  
    return navController
  }
    
  func presentBrowser(withURL url: String) {
    guard let URL = URL(string: url) else { return }
    DispatchQueue.main.async {
      if #available(iOS 10.0, *) {
        UIApplication.shared.open(URL as URL, options: [:], completionHandler: nil)
      } else {
        UIApplication.shared.openURL(URL as URL)
      }
    }
  }
    
  func presentDocViewer(withURL url: String) {
    guard let navController = R.storyboard.main.docViewerNavigationController() else {
      return
    }
    guard let viewerPdfVC = navController.viewControllers.first as? DocViewerViewController else {
      return
    }
    viewerPdfVC.mUrlString = url
    navController.modalTransitionStyle = .crossDissolve
    DispatchQueue.main.async {
      self.viewController?.present(navController, animated: true, completion: nil)
    }
  }
}
