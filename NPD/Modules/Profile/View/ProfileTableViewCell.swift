//
//  ProfileTableViewCell.swift
//  NPD
//
//  Created by Alejandro Tapia on 11/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    //MARK: - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
