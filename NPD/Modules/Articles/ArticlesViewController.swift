//
//  ArticlesViewController.swift
//  NPD
//
//  Created Alejandro Tapia on 27/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//
//  VIPER Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class ArticlesViewController: UIViewController, ArticlesViewProtocol {

  // MARK: - VIPER Variables
	var presenter: ArticlesPresenterProtocol?
    
  // MARK: - IBOutlets
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var pageControl: UIPageControl!
  
  // MARK: - Public variables
  public var moduleTitle: String!
  public var content: Content!
  
  // MARK: - Internal variables
  var urls: [String]!

  // MARK: -  Lifecycle
	override func viewDidLoad() {
    super.viewDidLoad()
    title = moduleTitle
    titleLabel.text = content.title
    pageControl.numberOfPages = content.articles.count
    scrollView.delegate = self
  }
    
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    addArtclesInHorizontalScrollView()
  }
    
  // MARK: - Internal
  func addArtclesInHorizontalScrollView() {
    for (index, article) in content.articles.enumerated() {
      let size =  scrollView.frame.size
      let label = setupLabel(fromSize: size, andIndex: index)
      label.text = article.contentDesc
      scrollView.addSubview(label)
      scrollView.contentSize = CGSize(width: size.width * CGFloat(index + 1), height: size.height)
    }
  
    // To setup right buttom item in first time
    if content.articles.count > 0 {
      updateRightButtonItem(withArticle: content.articles[0])
    }
  }
    
  func setupLabel(fromSize size: CGSize, andIndex index: Int) -> UILabel {
    let originX = size.width * CGFloat(index) + size.width * 0.1
    let frame = CGRect(x: originX, y: 0, width: size.width * 0.8, height: size.height)
    let label = UILabel(frame: frame)
    label.font = UIFont.npdLight17()
    label.textAlignment = .center
    label.numberOfLines = 0
    label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    return label
  }
  
  func updateRightButtonItem(withArticle article: Article){
    if article.urlDoc.isEmpty {
      navigationItem.rightBarButtonItem = nil
    } else {
      // Get number of URLs
      urls = article.urlDoc.components(separatedBy: ",")

      // Setup Right button
      let navRightButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_link"),
                                           style: .plain,
                                           target: self,
                                           action: #selector(actionFileButton))
      navRightButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
      navigationItem.rightBarButtonItem = navRightButton
    }
  }
    
  // MARK: - IBActions
  @IBAction func actionBackButton() {
    self.dismiss(animated: true, completion: nil)
  }
  
  @objc func actionFileButton() {
    if urls.count == 1 {
      // Check if is URL file or Browser URL
      let url = urls.first!
      switch Utils.checkResourceType(fromUrl: url) {
      case .doc:
          presenter?.willPresentDocViewer(withURL: url)
      case .web:
          presenter?.willPresentBrowser(withURL: url)
      }
    } else {
      // Show Lists of Docs View Controller
      presenter?.willPresentUrlsList(withURLs: urls)
    }
  }
}

// MARK: - UIScrollView delegate
extension ArticlesViewController: UIScrollViewDelegate {
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let pageNumber = Int(round(scrollView.contentOffset.x / scrollView.frame.size.width))
    updateRightButtonItem(withArticle: content.articles[pageNumber])
    pageControl.currentPage = Int(pageNumber)
  }
}
