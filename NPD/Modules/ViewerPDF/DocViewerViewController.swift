//
//  ViewerPDFViewController.swift
//  NPD
//
//  Created Alejandro Tapia on 17/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import UIKit

class DocViewerViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var noInternetConnectionView: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: - Public Variables
    public var mUrlString: String?
    
    // MARK: - Initialization
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.modalPresentationStyle = .currentContext
        self.modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Lifecycle
	override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        
        if Reachability.isConnectedToNetwork(){
            loadUrlInWebView()
        }else{
            noInternetConnectionView.alpha = 1.0
        }
    }
    
    // MARK: - Internal
    func loadUrlInWebView() {
        guard let urlString = mUrlString else {
            return
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        spinner.startAnimating()
        webView.loadRequest(URLRequest(url: url))
    }
    
    // MARK: - IBActions
    @IBAction func actionCloseButton() {
        if webView.isLoading {
            webView.stopLoading()
        }
        
        dismiss(animated: true, completion: nil)
    }
}

extension DocViewerViewController: UIWebViewDelegate {
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        spinner.stopAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        spinner.stopAnimating()
    }
}


