//
//  ModuleTableViewCell.swift
//  NPD
//
//  Created by Alejandro Tapia on 22/08/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//

import UIKit

class ModuleTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var subModuleImageView: UIImageView!
    @IBOutlet weak var subModuleTitleLabel: UILabel!
    
    //MARK: UITableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
