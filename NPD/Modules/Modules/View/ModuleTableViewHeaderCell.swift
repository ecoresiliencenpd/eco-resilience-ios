//
//  ModuleTableViewHeaderCell.swift
//  NPD
//
//  Created by Alejandro Tapia on 24/08/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//

import UIKit

class ModuleTableViewHeaderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
