//
//  ModulesPresenter.swift
//  NPD
//
//  Created Alejandro Tapia on 21/08/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class ModulesPresenter: ModulesPresenterProtocol {

    weak private var view: ModulesViewProtocol?
    var interactor: ModulesInteractorProtocol?
    private let router: ModulesWireframeProtocol

    init(interface: ModulesViewProtocol, interactor: ModulesInteractorProtocol?, router: ModulesWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
    func didPresentArticlesVC(withModuleTitle title: String, content: Content) {
        router.presentArticlesVC(withModuleTitle: title, content: content)
    }
}
