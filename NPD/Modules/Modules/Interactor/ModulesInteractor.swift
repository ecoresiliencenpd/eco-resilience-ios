//
//  ModulesInteractor.swift
//  NPD
//
//  Created Alejandro Tapia on 21/08/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class ModulesInteractor: ModulesInteractorProtocol {

    weak var presenter: ModulesPresenterProtocol?
}
