//
//  NewMainPresenter.swift
//  NPD
//
//  Created by Alex Tapia on 25/08/18.
//  Copyright © 2018 ECO-RESILENCE. All rights reserved.
//

import Foundation
import RealmSwift

class MainPresentable {
  
  // MARK: - Private Properties
  fileprivate var itself: Itself!
  
  // MARK: - Initializer
  init() {
    self.itself = RealmManager.fetchItself()
  }
}

// MARK: - NewMainPresenter Implementation
extension MainPresentable: MainPresenter {
  func fullname() -> String {
    guard let person = itself.person else { return "" }
    return "\(person.firstName) \(person.lastName)"
  }
  
  func titles() -> [String] {
    guard let section = itself.catalog?.sections else { return [String]() }
    return section.map{$0.name}
  }
  
  func profileName() -> String {
    guard let person = itself.person else { return "" }
    return person.profileName
  }
  
  func person() -> Person? {
    return itself.person
  }
  
  func modulesIn(section index: Int) -> List<Module>? {
    return itself.catalog?.sections[index].modules
  }
}
