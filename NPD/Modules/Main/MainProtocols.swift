//
//  NewMainProtocols.swift
//  NPD
//
//  Created by Alex Tapia on 25/08/18.
//  Copyright © 2018 ECO-RESILENCE. All rights reserved.
//

import Foundation
import RealmSwift

protocol MainPresenter {
  func fullname() -> String
  func profileName() -> String
  func titles() -> [String]
  func person() -> Person?
  func modulesIn(section index: Int) -> List<Module>?
}
