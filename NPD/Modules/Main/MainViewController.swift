//
//  NewMainViewController.swift
//  NPD
//
//  Created by Alex Tapia on 25/08/18.
//  Copyright © 2018 ECO-RESILENCE. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController {
  
  // MARK: - Outlets
  @IBOutlet private weak var welcomeLabel: UILabel!
  @IBOutlet private weak var profileNameLabel: UILabel!
  // Container Views
  @IBOutlet private weak var twoOptionsView: UIStackView!
  @IBOutlet private weak var fourOptionsView: UIStackView!
  // Labels
  @IBOutlet private weak var twoOptionsTitleOneLabel: UILabel!
  @IBOutlet private weak var twoOptionsTitleTwoLabel: UILabel!
  @IBOutlet private weak var fourOptionsTitleOneLabel: UILabel!
  @IBOutlet private weak var fourOptionsTitleTwoLabel: UILabel!
  @IBOutlet private weak var fourOptionsTitleThreeLabel: UILabel!
  @IBOutlet private weak var fourOptionsTitleFourLabel: UILabel!
  // Option Views
  @IBOutlet private weak var twoOptionsOptionOneView: UIView!
  @IBOutlet private weak var twoOptionsOptionTwoView: UIView!
  @IBOutlet private weak var fourOptionsOptionOneView: UIView!
  @IBOutlet private weak var fourOptionsOptionTwoView: UIView!
  @IBOutlet private weak var fourOptionsOptionThreeView: UIView!
  @IBOutlet private weak var fourOptionsOptionFourView: UIView!
  
  // MARK: - Private Properties
  fileprivate var presenter: MainPresenter!

  // MARK: - Override Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    presenter = MainPresentable()
    setupUI()
  }
  
  // MARK: - Private Methods
  private func setupUI() {
    welcomeLabel.text = "Welcome, \(presenter.profileName())"
    profileNameLabel.text = presenter.fullname()
    setupTitles()
  }
  
  private func setupTitles() {
    let titles = presenter.titles()
    if titles.count == 4 {
      twoOptionsView.removeFromSuperview()
      fourOptionsTitleOneLabel.text = titles[0].uppercased()
      fourOptionsTitleTwoLabel.text = titles[1].uppercased()
      fourOptionsTitleThreeLabel.text = titles[2].uppercased()
      fourOptionsTitleFourLabel.text = titles[3].uppercased()
    } else if titles.count == 2 {
      fourOptionsView.removeFromSuperview()
      twoOptionsTitleOneLabel.text = titles[0]
      twoOptionsTitleTwoLabel.text = titles[0]
    }
  }
  
  // MARK: - Fileprivate Methods
  fileprivate func presentModulesFrom(section index: Int) {
    guard let modules = presenter.modulesIn(section: index) else { return }
    let title = presenter.titles()[index]
    let modulesTableVC = ModulesRouter.createModule(with: title, and: modules)
    modulesTableVC.modalTransitionStyle = .crossDissolve
    present(modulesTableVC, animated: true, completion: nil)
  }
  
  // MARK: - Actions
  @IBAction func selectedMenuButton() {
    let slideMenuVC = SlideMenuRouter.createModule(withDelegate: self)
    slideMenuVC.sectionNames = presenter.titles()
    slideMenuVC.profileName = presenter.profileName()
    present(slideMenuVC, animated: false, completion: nil)
  }
  
  @IBAction func selectedOptionView(sender: UITapGestureRecognizer) {
    guard let view = sender.view else { return }
    switch view {
    case twoOptionsOptionOneView, fourOptionsOptionOneView:
      presentModulesFrom(section: 0)
    case twoOptionsOptionTwoView, fourOptionsOptionTwoView:
      presentModulesFrom(section: 1)
    case fourOptionsOptionThreeView:
      presentModulesFrom(section: 2)
    case fourOptionsOptionFourView:
      presentModulesFrom(section: 3)
    default:
      break
    }
  }
}

//MARK: - SlideMenuViewControllerDelegate
extension MainViewController: SlideMenuViewControllerDelegate {
  func slideMenu(didSelectItemAt index: Int) {
    presentModulesFrom(section: index)
  }
  
  func slideMenuDidSelectAbout() {
    let aboutVC = AboutRouter.createModule()
    aboutVC.modalTransitionStyle = .crossDissolve
    present(aboutVC, animated: true, completion: nil)
  }
  
  func slideMenuDidSelectMyProfile() {
    guard let profileVC = ProfileRouter.createModule() as? ProfileViewController else { return }
    guard let person = presenter.person() else { return }
    profileVC.person = person
    present(profileVC, animated: true, completion: nil)
  }
  
  func slideMenuDidSelectLogout() {
    let alert = UIAlertController(title: nil, message: "Logout Confirmation", preferredStyle: .alert)
    let actionOK = UIAlertAction(title: "Logout", style: .destructive) {_ in
      UserDefaults.standard.set(false, forKey: Constants.USER_DEFAULTS_IS_LOGGED_KEY)
      RealmManager.deleteAll()
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      appDelegate.window?.rootViewController = LoginRouter.createModule()
    }
    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
    alert.addAction(actionOK)
    self.present(alert, animated: true, completion: nil)
  }
}
