//
//  SlideMenuInteractor.swift
//  NPD
//
//  Created Alejandro Tapia on 03/09/17.
//  Copyright © 2017 ECO-RESILENCE. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class SlideMenuInteractor: SlideMenuInteractorProtocol {

    weak var presenter: SlideMenuPresenterProtocol?
}
