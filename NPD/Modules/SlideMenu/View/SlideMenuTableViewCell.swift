//
//  SlideMenuTableViewCell.swift
//  NPD
//
//  Created by Alejandro Tapia on 03/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//

import UIKit

class SlideMenuTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        iconImageView.tintColor = selected ? #colorLiteral(red: 0, green: 0.2156862745, blue: 0.4509803922, alpha: 1): #colorLiteral(red: 0.3918663859, green: 0.0311193075, blue: 0, alpha: 1)
        titleLabel.textColor = selected ? #colorLiteral(red: 0, green: 0.2156862745, blue: 0.4509803922, alpha: 1) : #colorLiteral(red: 0.3918663859, green: 0.0311193075, blue: 0, alpha: 1)
    }
    
    /// The `UINib` object initialized for the cell.
    ///
    /// - Returns: The initialized `UINib` object.
    static func nib() -> UINib {
        return UINib(
            nibName: String(describing: classForCoder()),
            bundle: Bundle(for: classForCoder())
        )
    }
    
    ///  The default string used to identify a reusable cell for text message items.
    ///
    ///  - Returns: The string used to identify a reusable cell.
    static func cellReuseIdentifier() -> String {
        return String(describing: classForCoder())
    }
}
