//
//  SlideMenuViewController.swift
//  NPD
//
//  Created Alejandro Tapia on 03/09/17.
//  Copyright © 2017 ECO-RESILIENCE. All rights reserved.
//
//  VIPER Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

protocol SlideMenuViewControllerDelegate {
    func slideMenu(didSelectItemAt index: Int)
    func slideMenuDidSelectMyProfile()
    func slideMenuDidSelectAbout()
    func slideMenuDidSelectLogout()
}

class SlideMenuViewController: UIViewController, SlideMenuViewProtocol {

    // MARK: - VIPER Variables
	var presenter: SlideMenuPresenterProtocol?

    // MARK: - IBOutlet
    @IBOutlet weak var bgTransparent: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var leftTableViewConstraint: NSLayoutConstraint!
    
    // MARK: - Constants
    let cellIdentifier = SlideMenuTableViewCell.cellReuseIdentifier()
    
    // MARK: - Public properties
    public var delegate: SlideMenuViewControllerDelegate?
    public var sectionNames = [String]()
    public var profileName = ""
    
    // MARK: - Variables
    var beganPanLocationX: CGFloat!

    //MARK: - Initialize
    public init() {
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self)))
        setupViewController()
    }
    
    public init(withDelegate delegate:SlideMenuViewControllerDelegate) {
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self)))
        self.delegate = delegate
        setupViewController()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Lifecycle
	override func viewDidLoad() {
        super.viewDidLoad()
        // Add Item Profile and Logout
        sectionNames.append(contentsOf: ["My Profile", "About us", "Logout"])
        setTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupInitialViewsState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showViewWithAnimation()
    }
    
    // MARK: - Internal
    func setupViewController() {
        self.modalTransitionStyle = .crossDissolve
        self.modalPresentationStyle = .overCurrentContext
    }
    
    func setupInitialViewsState() {
        bgTransparent.alpha = 0.0
        leftTableViewConstraint.constant = -tableView.frame.width
        view.layoutIfNeeded()
    }
    
    func showViewWithAnimation() {
        leftTableViewConstraint.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.bgTransparent.alpha = 0.35
            self.view.layoutIfNeeded()
        }
    }
    
    func setTableView() {
        tableView.register(SlideMenuTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        tableView.rowHeight = 64
    }
    
    func dismiss(withSelected index: Int?) {
        leftTableViewConstraint.constant = -tableView.frame.width
        UIView.animate(withDuration: 0.25, animations: {
            self.bgTransparent.alpha = 0.0
            self.view.layoutIfNeeded()
        }) {_ in
            self.dismiss(animated: false) {
                if let index = index {
                    self.responseDelegateFromIndex(index)
                }
            }
        }
    }
    
    func responseDelegateFromIndex(_ index: Int) {
        if index == (sectionNames.endIndex - 1) {
            self.delegate?.slideMenuDidSelectLogout()
        } else if index == (sectionNames.endIndex - 2) {
            self.delegate?.slideMenuDidSelectAbout()
        } else if index == (sectionNames.endIndex - 3) {
            self.delegate?.slideMenuDidSelectMyProfile()
        } else {
            self.delegate?.slideMenu(didSelectItemAt: index)
        }
    }
    
    func getImageOfTitle(_ title: String) -> UIImage? {
        switch title {
        case _ where title.hasSuffix("competency"):
          return R.image.ic_student()
        case "Self":
            return getImageSelfOfProfileName()
        case "Principal", "Pricipal":
            return R.image.ic_principal()
        case "Parent":
            return R.image.ic_parent()
        case "Student":
            return R.image.ic_student()
        case "Teacher":
            return R.image.ic_teacher()
        case "My Profile":
            return R.image.ic_my_profile()
        case "About us":
            return R.image.ic_about()
        case "Logout":
            return R.image.ic_logout()
        default:
            return nil
        }
    }
    
    func getImageSelfOfProfileName() -> UIImage? {
        switch profileName {
        case "Principal", "Pricipal":
            return R.image.ic_principal()
        case "Teacher":
            return R.image.ic_teacher()
        case "Parent":
            return R.image.ic_parent()
        case "Student":
            return R.image.ic_student()
        default:
            return nil
        }
    }
    
    // MARK: - IBActions
    @IBAction func actionBackgroundTapGesture() {
        dismiss(withSelected: nil)
    }
    
    @IBAction func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        if panGesture.state == .began {
            beganPanLocationX = panGesture.location(in: view).x
        } else if panGesture.state == .changed {
            let panTranslation = panGesture.translation(in: view)
            let newTranslationX = leftTableViewConstraint.constant + panTranslation.x
            if newTranslationX >= -(tableView.frame.width) && newTranslationX <= 0 {
                leftTableViewConstraint.constant = newTranslationX
            }
            panGesture.setTranslation(CGPoint.zero, in: view)
        } else if panGesture.state == .ended {
            let endedPanLocationX = panGesture.location(in: view).x
            if endedPanLocationX < beganPanLocationX { // Close (dismiss)
                self.dismiss(withSelected: nil)
            } else {
                leftTableViewConstraint.constant = 0
                UIView.animate(withDuration: 0.15) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}

// MARK: - UITableView data source
extension SlideMenuViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SlideMenuTableViewCell
        let title = sectionNames[indexPath.row]
        cell.titleLabel.text = title
        cell.iconImageView.image = getImageOfTitle(title)
        return cell
    }
}

// MARK: - UITableView delegate
extension SlideMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(withSelected: indexPath.row)
    }
}
